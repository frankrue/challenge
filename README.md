# "Project Dashboard" Challenge

### Installation and Development

Installs all necessaries components and compiles into `public` (static assets)
and `.tmp` (compiled HTML, CSS, and JS) folders for gulp "watched" development.

```bash
npm install
gulp
```

### Distribution

Creates a `build` folder that can be zipped for production.

```bash
gulp build
```
