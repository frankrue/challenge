var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

// dev tasks
gt('clean-tmp');
gt('copy-fonts');
gt('browserify-dev');
gt('sass-dev');
gt('pug-dev');
gt('browser-sync', ['copy-fonts','pug-dev','sass-dev','browserify-dev'] );

// gulp.task('clean-tmp', require('./gulp-tasks/clean-tmp')(gulp,plugins));
// gulp.task('copy-fonts', ['clean-tmp'], require('./gulp-tasks/copy-fonts')(gulp, plugins));
// gulp.task('browserify-init', ['clean-tmp'], require('./gulp-tasks/browserify-dev')(gulp, plugins));
// gulp.task('sass-init', ['clean-tmp'], require('./gulp-tasks/sass-dev')(gulp, plugins));
// gulp.task('pug-init', ['clean-tmp'], require('./gulp-tasks/pug-dev')(gulp, plugins));
// gulp.task('sass-dev', require('./gulp-tasks/sass-dev')(gulp, plugins));
// gulp.task('pug-dev', require('./gulp-tasks/pug-dev')(gulp, plugins));
// gulp.task('browser-sync', ['copy-font-awesome-init','pug-init','sass-init','browserify-init'],
//   require('./gulp-tasks/browser-sync')(gulp, plugins));

// // dist tasks
gt('clean-build');
gt('browserify-dist');
gt('sass-dist');
gt('pug-dist');
gt('copy-public-dist',['copy-fonts','clean-build']);

// serve and watch
gulp.task('serve',['browser-sync'], function() {
  gulp.watch('./source/pug/**/*.pug', ['pug-dev']);
  gulp.watch('./source/sass/**/*.{sass,scss}', ['sass-dev']);
});

// macros
gulp.task('default',['serve']);
gulp.task('build',['clean-build','browserify-dist','pug-dist','sass-dist','copy-public-dist']);

function gt(taskName, requirements) {
  gulp.task(taskName, requirements, require('./gulp-tasks/' + taskName)(gulp,plugins));
}
