var del = require('del');
module.exports = function cleanBuildWrapper(gulp, plugins) {
  return function cleanBuild() {
    return del([
      './build/**/*'
    ]);
  };
};
