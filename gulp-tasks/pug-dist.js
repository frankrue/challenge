module.exports = function pugDistWrapper(gulp, plugins) {
  return function pugDist() {
    var DEST = 'build';
    return gulp
      .src(['./source/pug/**/*.pug'], { base: './source/pug' })
      .pipe(plugins.changed(DEST))
      .pipe(plugins.pug({ pretty: true }))
      .pipe(gulp.dest(DEST));
  };
};
