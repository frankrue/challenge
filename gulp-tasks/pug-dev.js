module.exports = function pugDevWrapper(gulp, plugins) {
  return function pugDev() {
    var DEST = '.tmp';
    return gulp
      .src(['./source/pug/**/*.pug'], { base: './source/pug' })
      .pipe(plugins.changed(DEST))
      .pipe(plugins.pug({ pretty: true }))
      .pipe(gulp.dest(DEST));
  };
};
