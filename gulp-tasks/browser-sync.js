module.exports = function bsWrapper(gulp, plugins) {

  return function bs() {

    var browserSync = require('browser-sync');
    var reload = browserSync.reload;

    browserSync.init({
      browser: 'google chrome',
      server: {
        baseDir: ['.tmp','public']
      }
    });

    return gulp.watch([
      '.tmp/**/*.html',
      'public/**/*',
      '.tmp/js/*.js'
    ]).on('change', reload);

  };

};
