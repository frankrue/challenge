var del = require('del');
module.exports = function cleanTmpWrapper(gulp, plugins) {
  return function cleanTmp() {
    return del([
      './.tmp/**/*'
    ]);
  };
};
