module.exports = function copyFontsWrapper(gulp, plugins) {
  return function copyFonts() {
    var DEST = 'public/fonts';
    return gulp
      .src(['./node_modules/font-awesome/fonts/**'])
      .pipe(plugins.changed(DEST))
      .pipe(gulp.dest(DEST));
  };
};
