module.exports = function copyPublicDistWrapper(gulp, plugins) {
  return function copyPublicDist() {
    var DEST = 'build';
    return gulp
      .src(['./public/**/*'], { base: 'public' })
      .pipe(plugins.changed(DEST))
      .pipe(gulp.dest(DEST));
  };
};
