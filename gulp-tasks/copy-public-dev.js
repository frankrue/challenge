module.exports = function copyPublicDevWrapper(gulp, plugins) {
  return function copyPublicDev() {
    var DEST = '.tmp';
    return gulp
      .src(['./public/**/*'], { base: 'public' })
      .pipe(plugins.changed(DEST))
      .pipe(gulp.dest(DEST));
  };
};
