(function(){

  var angular = require('angular');
  var defaults = require('./defaults');

  configureApplication();
  loadModules();
  bindApplication();


  function configureApplication() {
    angular.module(defaults.app.name,[
      require('angular-ui-bootstrap'),
      require('angular-ui-router'),
      require('angular-bootstrap-datetimepicker'),
      require('angularjs-slider')
    ]);
  }

  function loadModules() {
    require('./controllers/Dashboard')(angular, defaults);
    require('./directives/pd-record')(angular, defaults);
    require('./directives/filter-bar')(angular, defaults);
    require('./services/Data')(angular, defaults);
    require('./config.js')(angular, defaults);
    require('./run.js')(angular, defaults);
  }

  function bindApplication() {
    angular.bootstrap(document,[defaults.app.name]);
  }

})();
