module.exports = function Results(angular, defaults) {

  angular.module(defaults.app.name)
    .controller('DashboardController', Dashboard);

  function Dashboard(DataService, $filter, $uibModal) {

    var vm = this;

    setupTriggers();
    setupDefaults();
    setupResults();

    function setupTriggers() {
      vm.getSortBy = getSortBy;
      vm.setSortBy = setSortBy;
      vm.getDisplayLabel = getDisplayLabel;
      vm.setFilterStatus = setFilterStatus;
      vm.getFilter = getFilter;
      vm.getOrderBy = getOrderBy;
      vm.getColFromRecord = getColFromRecord;
      vm.getTotalByStatus = getTotalByStatus;
      vm.getTotalFromSet = getTotalFromSet;
      vm.reset = reset;
    }

    function setupDefaults() {

      var nowDate = new Date();
      var thenDate = new Date( nowDate.valueOf() - 1000 * 60 * 60 * 24 * 365 * 2 );

      vm.labels = [];
      for (var key in DataService.labels) vm.labels.push(key);

      vm.filter = {
        startDateTime: thenDate,
        endDateTime: nowDate,
        status: "",
        search: "",
        budgetMin: 0,
        budgetMax: 100000
      };

      vm.sortBy = {
        label: 'created',
        direction: '-'
      };

      vm.statusLabels = {
        'archived': 'Archived',
        'working': 'Working',
        'delivered': 'Delivered',
        'new': 'New'
      };

    }

    function setupResults() {
      vm.results = DataService.results;
    }

    function getSortBy() {
      return vm.sortBy;
    }

    function setSortBy(label) {
      if (vm.sortBy.direction === '+' && vm.sortBy.label === label) vm.sortBy.direction = '-';
      else vm.sortBy.direction = '+';
      vm.sortBy.label = label;
    }

    function getDisplayLabel(label) {
      return DataService.labels[label];
    }

    function setFilterStatus(status) {
      vm.filter.status = status;
    }

    function getFilter(item, index, array) {

      if (
        itemIsInDateRange() &&
        itemIsInStatusFilter() &&
        itemIsInSearchFilter() &&
        itemIsInBudgetFilter()
      ) {
        return true;
      } else {
        return false;
      }

      function itemIsInDateRange() {
        var created = new Date(item.created);
        return (created.valueOf() <= vm.filter.endDateTime.valueOf() && created.valueOf() >= vm.filter.startDateTime.valueOf());
      }

      function itemIsInStatusFilter() {
        return (vm.filter.status === "" || item.status === vm.filter.status);
      }

      function itemIsInSearchFilter() {
        var searchText = vm.filter.search.toLowerCase();
        return (
          item.title.toLowerCase().indexOf(searchText) > -1 ||
          item.division.toLowerCase().indexOf(searchText) > -1 ||
          item.project_owner.toLowerCase().indexOf(searchText) > -1
        )
      }

      function itemIsInBudgetFilter() {
        return (item.budget <= vm.filter.budgetMax && item.budget >= vm.filter.budgetMin);
      }

    }

    function getOrderBy() {
      return vm.getSortBy().direction + vm.getSortBy().label;
    }

    function getColFromRecord(record, label) {
      switch(label) {

        case 'budget':
        return '$ ' + $filter('number')(record[label], 2)

        default:
        return record[label];

      }
    }

    function getTotalByStatus(results, status) {
      if (results) {
        var filtered = results.filter(function(item) {
          if (item.status === status) return true;
          return false;
        });
        return filtered.length;
      }
      return 0;
    }

    function getTotalFromSet(results) {
      if (results) {
        var reduced = results.reduce(function(acc, item) {
          var summing = acc + item.budget;
          return Math.round(summing * 100) / 100;
        }, 0);
        return reduced;
      }
      return 0;
    }

    function reset() {
      var inst = $uibModal.open({
        template: '<div class="modal-header"><h3 class="modal-title text-center">Are you sure?</h4></div><div class="modal-footer text-center"><button class="btn btn-sm btn-danger" ng-click="$close()">Yes</button><button class="btn btn-sm btn-default" ng-click="$dismiss()">No</button></div>',
        size: 'sm',
        windowClass: 'modal-confirm'
      });
      inst.result.then(function ConfirmYes() {
        setupDefaults();
      });
    }

  }

};
