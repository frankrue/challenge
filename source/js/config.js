module.exports = function Results(angular, defaults) {

  angular.module(defaults.app.name)
    .config(Config);

  function Config($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('dashboard',{
        url: '/',
        templateUrl: 'views/Dashboard.html',
        controller: 'DashboardController',
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('/');

  }

};
