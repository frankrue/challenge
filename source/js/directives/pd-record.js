module.exports = function Results(angular, defaults) {

  angular.module(defaults.app.name)
    .directive('pdRecord', PDRecord);

  function PDRecord() {
    return {
      restrict: 'A',
      scope: {
        pdRecord: '<',
        labels: '<',
        sortBy: '=',
        index: '@',
        statusLabels: '<'
      },
      templateUrl: "directives/pd-record.html",
      controller: Result,
      controllerAs: 'vm',
      bindToController: true
    };
  }

  function Result($filter, $scope, $rootScope, $element, $uibModal) {

    var vm = this;

    setupTriggers();
    setupDefaults();
    setupListeners();

    function setupTriggers() {
      vm.getCol = getCol;
      vm.quickEdit = quickEdit;
      vm.setRecordPair = setRecordPair;
      vm.updateModified = updateModified;
      vm.closeQuickEdit = closeQuickEdit;
      vm.open = openModal;
    }

    function setupDefaults() {
      vm.editing = false;
      vm.dirty = false;
    }

    function setupListeners() {
      $scope.$on('pd.editing', function(event, data) {

        if (data.recordIndex === -1) {
          $element.removeClass('blurred editing');
        } else {

          if (data.recordIndex === vm.index) {
            $element.addClass('editing');
            $element.removeClass('blurred');
          } else {
            updateDirtyStatus();
            $element.addClass('blurred');
            $element.removeClass('editing');
          }

        }

      });
    }

    function getCol(label) {
      switch(label) {

        case 'budget':
        return '$ ' + $filter('number')(vm.pdRecord[label], 2)

        default:
        return vm.pdRecord[label];

      }
    }

    function quickEdit() {
      if (!vm.editing) {
        $element.removeClass('saved');
        vm.editing = true;
        $rootScope.$broadcast('pd.editing',{ recordIndex: vm.index });
      }
    }

    function closeQuickEdit() {
      updateDirtyStatus()
      $rootScope.$broadcast('pd.editing',{ recordIndex: -1 });
    }

    function updateDirtyStatus() {
      vm.editing = false;
      if (vm.dirty) {
        vm.dirty = false;
        $element.addClass('saved');
      }
    }

    function setRecordPair(label, value) {
      vm.pdRecord[label] = value;
      vm.updateModified();
    }

    function updateModified() {
      vm.dirty = true;
      vm.pdRecord.modified = $filter('date')(new Date(), 'M/d/yyyy');
    }

    function openModal() {
      $uibModal.open({
        template: '<div class="modal-header"><h3 class="modal-title">Edit Record (Detail View)</h4></div><div class="modal-body">This would move either to a modal or <br />new screen to edit an existing record in more detail.</div><div class="modal-footer"><button class="btn btn-sm btn-danger" ng-click="$close()">Close</button></div>',
        size: 'sm'
      });
    }

  }

};
