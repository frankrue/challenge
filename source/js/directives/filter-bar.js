module.exports = function FilterBar(angular, defaults) {

  angular.module(defaults.app.name)
    .directive('filterBar', FilterBarDirective);

  function FilterBarDirective() {
    return {
      restrict: 'E',
      scope: {
        filter: '=',
        labels: '<',
        statusLabels: '<',
        reset: '&'
      },
      templateUrl: "directives/filter-bar.html",
      controller: FilterBarController,
      controllerAs: 'vm',
      bindToController: true
    }
  }

  function FilterBarController($filter, DataService, $uibModal) {

    var vm = this;

    setupTriggers();
    setupConfigurations();

    function setupTriggers() {
      vm.getDisplayLabel = getDisplayLabel;
      vm.getDisplayStatus = getDisplayStatus;
      vm.setFilterStatus = setFilterStatus;
      vm.export = datasetExport;
      vm.add = add;
    }

    function setupConfigurations() {

      vm.dateTimeConfig = {
        startView: 'day',
        minView: 'day'
      };

      vm.sliderConfig = {
        floor: 0,
        ceil: 100000,
        step: 1000,
        translate: function(value) { return '$' + $filter('number')(value, 2) }
      }

    }

    function getDisplayLabel(label) {
      return DataService.labels[label];
    }

    function getDisplayStatus(status) {
      if (status === "") return "Show All";
      return vm.statusLabels[status];
    }

    function setFilterStatus(status) {
      vm.filter.status = status;
    }

    function add() {
      $uibModal.open({
        template: '<div class="modal-header"><h3 class="modal-title">Add a Record</h4></div><div class="modal-body">This would move either to a modal or new screen to add a brand new record.</div><div class="modal-footer"><button class="btn btn-sm btn-danger" ng-click="$close()">Close</button></div>',
        size: 'sm'
      });
    }

    function datasetExport(type) {
      var n = "";
      if (type === "excel") n = "n";
      $uibModal.open({
        template: '<div class="modal-header"><h3 class="modal-title">Export Dataset</h4></div><div class="modal-body">This would export the current dataset as a' + n + ' <strong class="text-uppercase">' + type + '</strong> file.</div><div class="modal-footer"><button class="btn btn-sm btn-danger" ng-click="$close()">Close</button></div>',
        size: 'sm'
      });
    }


  }

};
