module.exports = function Results(angular, defaults) {

  angular.module(defaults.app.name)
    .run(RunOnce);

  function RunOnce($templateCache) {

    clearVendorTemplates();


    function clearVendorTemplates() {
      $templateCache.remove('templates/datetimepicker.html');
    }

  }

};
